--Introduction--

A repository of scripts I've written to address things I need to do.

They're written from my needs, my requirements, but if they're useful or helpful to you, then knock yourself out.

No warranty, etc, so if they cause the sad and early demise of your cat, that's unfortunate, but not my fault.

--Scripts--

  dsFromKey

  This script takes a domain as an argument and for each DNSKEY found, checks and reports if the associated DS is published on the parents NS


  checkKeyFromDs

  This does the reverse of the above; it takes a domain as an argument and for each DS published in the parents' NS, it checks
  whether the associated DNSKEY is published in the domain's NS


  checkDelegationMatchesZone

  As it says on the tin; takes a domain as an argument and checks whether the domain's NS RRSET matches the delegation NS RRSET in the parents' NS

  If it gets an ANSWER from the parent, it assumes the domain and its parent are auth on the same server and doesn't continue.
